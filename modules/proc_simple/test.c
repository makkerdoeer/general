#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>


MODULE_AUTHOR("Makker Doeer <makker.doeer@gmail.com>");
MODULE_LICENSE("none");
MODULE_DESCRIPTION("...");
MODULE_INFO(vermagic,"4.9.0-3-amd64 SMP mod_unload modversions ");

static void __exit exit_test(void) {
    printk(KERN_ALERT "Exiting test module by Makker Doeer\n");
}
static int __init init_test(void) {
    printk(KERN_ALERT "Loading test module by Makker Doeer\n");
    return 0;
}



module_init(init_test);
module_exit(exit_test);


